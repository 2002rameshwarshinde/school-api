package org.dnyanyog.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import javax.xml.xpath.XPathExpressionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

public class TeacherControllerTest extends AbstractTestNGSpringContextTests  {

	@Autowired
	MockMvc mocMvc;

	@Test (priority=1)//new Entry
	public void verifyUsersSignupSuccessOpertionExceptionObject() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/Teacher/api/add").content("<TeacherRequest>\r\n"
				+ "    <data>\r\n"
				+ "        <name>John Doe</name>\r\n"
				+ "        <mobileNo>1234567890</mobileNo>\r\n"
				+ "        <address>123 Main Street</address>\r\n"
				+ "        <qualification>Bachelor of Education</qualification>\r\n"
				+ "        <subject>Mathematics</subject>\r\n"
				+ "        <gender>Male</gender>\r\n"
				+ "        <email>johndoe@example.com</email>\r\n"
				+ "    </data>\r\n"
				+ "</TeacherRequest>")
				
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder)
				.andExpect(status().isCreated())
				.andExpect(xpath("/SignUpResponse/status").string("success"))
				.andExpect(xpath("/SignUpResponse/message").string("user account created successfully"))
				.andExpect(xpath("/SignUpResponse/data/fullName").string("sagar 123"))
				.andExpect(xpath("/SignUpResponse/data/email").string("sagar12@example.com"))		
				.andExpect(xpath("/SignUpResponse/data/currency").string("USD"))
				.andExpect(xpath("/SignUpResponse/data/language").string("en"))
				.andExpect(xpath("/SignUpResponse/data/country").string("IN"))
			//	.andExpect(xpath("/SignUpResponse/data/userId").string("2"))
				.andReturn();
	}
}
