package org.dnyanyog.entity;

import javax.persistence.Entity;

import org.springframework.stereotype.Service;



public enum LeaveStatus {
	 PENDING, // Leave request is pending approval
	APPROVED, // Leave request is approved
	REJECTED // Leave request is rejected
;

	
}
