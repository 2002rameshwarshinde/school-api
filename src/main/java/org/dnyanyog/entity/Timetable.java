package org.dnyanyog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Table
@Entity
@Component
public class Timetable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column 
	private String timeSlot;

	@Column
	private String className;
	
	@Column
	private String monDay;
	
	@Column
	private String tuesDay;
	
	@Column
	private String wednesDay;
	
	@Column
	private String thursDay;
	
	@Column
	private String friDay;
	
	@Column
	private String saturDay;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTimeSlot() {
		return timeSlot;
	}

	public void setTimeSlot(String timeSlot) {
		this.timeSlot = timeSlot;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMonDay() {
		return monDay;
	}

	public void setMonDay(String monDay) {
		this.monDay = monDay;
	}

	public String getTuesDay() {
		return tuesDay;
	}

	public void setTuesDay(String tuesDay) {
		this.tuesDay = tuesDay;
	}

	public String getWednesDay() {
		return wednesDay;
	}

	public void setWednesDay(String wednesDay) {
		this.wednesDay = wednesDay;
	}

	public String getThursDay() {
		return thursDay;
	}

	public void setThursDay(String thursDay) {
		this.thursDay = thursDay;
	}

	public String getFriDay() {
		return friDay;
	}

	public void setFriDay(String friDay) {
		this.friDay = friDay;
	}

	public String getSaturDay() {
		return saturDay;
	}

	public void setSaturDay(String saturDay) {
		this.saturDay = saturDay;
	}

	
}
