package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class TimetableRequest {
	private String timeslot;
	private String Classname;
	private String MonDay;
	private String tuesDay;
	private String WednesDay;
	private String ThursDay;
	private String friDay;
	private String SaturDay;
	
	
	public String getTimeslot() {
		return timeslot;
	}
	public void setTimeslot(String timeslot) {
		this.timeslot = timeslot;
	}
	public String getClassname() {
		return Classname;
	}
	public void setClassname(String classname) {
		Classname = classname;
	}
	public String getMonDay() {
		return MonDay;
	}
	public void setMonDay(String monDay) {
		MonDay = monDay;
	}
	public String getTuesDay() {
		return tuesDay;
	}
	public void setTuesDay(String tuesDay) {
		this.tuesDay = tuesDay;
	}
	public String getWednesDay() {
		return WednesDay;
	}
	public void setWednesDay(String wednesDay) {
		WednesDay = wednesDay;
	}
	public String getThursDay() {
		return ThursDay;
	}
	public void setThursDay(String thursDay) {
		ThursDay = thursDay;
	}
	public String getFriDay() {
		return friDay;
	}
	public void setFriDay(String friDay) {
		this.friDay = friDay;
	}
	public String getSaturDay() {
		return SaturDay;
	}
	public void setSaturDay(String saturDay) {
		SaturDay = saturDay;
	}
	
}
