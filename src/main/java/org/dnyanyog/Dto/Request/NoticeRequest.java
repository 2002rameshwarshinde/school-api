package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class NoticeRequest {
	private String Date;
	private String notice;
	private String createdby;

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

}
