package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

@Service
public class AdmissionRequest {

	
	private String StudentName;

	private String fatherName;

	private String MotherName;

	private String Lastname;

	private String parentMobileNo;

	private String Address;

	private String BirthDate;

	private String Gender;

	private String AppliedClass;

	private String PreviousSchoolName;

	private String studentMob;

	private String email;

	public String getStudentName() {
		return StudentName;
	}

	public void setStudentName(String studentName) {
		StudentName = studentName;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getMotherName() {
		return MotherName;
	}

	public void setMotherName(String motherName) {
		MotherName = motherName;
	}

	public String getLastname() {
		return Lastname;
	}

	public void setLastname(String lastname) {
		Lastname = lastname;
	}

	public String getParentMobileNo() {
		return parentMobileNo;
	}

	public void setParentMobileNo(String parentMobileNo) {
		this.parentMobileNo = parentMobileNo;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getBirthDate() {
		return BirthDate;
	}

	public void setBirthDate(String birthDate) {
		BirthDate = birthDate;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public String getAppliedClass() {
		return AppliedClass;
	}

	public void setAppliedClass(String appliedClass) {
		AppliedClass = appliedClass;
	}

	public String getPreviousSchoolName() {
		return PreviousSchoolName;
	}

	public void setPreviousSchoolName(String previousSchoolName) {
		PreviousSchoolName = previousSchoolName;
	}

	public String getStudentMob() {
		return studentMob;
	}

	public void setStudentMob(String studentMob) {
		this.studentMob = studentMob;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
