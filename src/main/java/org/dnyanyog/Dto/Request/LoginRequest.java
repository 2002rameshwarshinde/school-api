package org.dnyanyog.Dto.Request;

import org.springframework.stereotype.Service;

@Service
public class LoginRequest {
 
	private String MobileNo;
	
	private String Password;

	public String getMobileNo() {
		return MobileNo;
	}

	public void setMobileNo(String mobileNo) {
		MobileNo = mobileNo;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}
}
