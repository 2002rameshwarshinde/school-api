package org.dnyanyog.Dto.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
@Service
public class AdmissionResponse {
private String status;
private String Message;
@Autowired
private StudentData Data;
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public String getMessage() {
	return Message;
}
public void setMessage(String message) {
	Message = message;
}
public StudentData getData() {
	return Data;
}
public void setData(StudentData data) {
	Data = data;
}


}
