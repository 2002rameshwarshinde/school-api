package org.dnyanyog.service;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.Dto.Request.DiaryRequest;
import org.dnyanyog.Dto.response.DiaryData;
import org.dnyanyog.Dto.response.DiaryResponse;
import org.dnyanyog.Repository.DiaryRepository;
import org.dnyanyog.entity.DiaryInfo;
import org.dnyanyog.entity.LeaveApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class DiaryService {

	@Autowired
	DiaryRepository diaryrepo;
	
	@Autowired
	DiaryInfo diaryinfo;
	
	@Autowired
	DiaryResponse response;
	
	
	public ResponseEntity<DiaryResponse>addInfo(@RequestBody DiaryRequest request){
		
		
		response=new DiaryResponse();
		response.setData(new DiaryData());
		
		diaryinfo=new DiaryInfo();
		
		diaryinfo.setGrnNO(request.getGrnNO());
		diaryinfo.setStudentName(request.getStudentName());
		diaryinfo.setInformation(request.getInformation());
		diaryinfo.setDate(request.getDate());
		
		diaryinfo=diaryrepo.save(diaryinfo);
		
		response.setStatus("Success");
		response.setMessage("Notification sent to parent ");
		response.getData().setId(diaryinfo.getId());
		response.getData().setGrnNO(diaryinfo.getGrnNO());
		response.getData().setStudentName(diaryinfo.getStudentName());
		response.getData().setInformation(diaryinfo.getInformation());
		response.getData().setDate(diaryinfo.getDate());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
	
	public List<DiaryInfo>findbyGrnNO(Long grnNO){
			
		return diaryrepo.findByGrnNO(grnNO);
	}
	
	public DiaryInfo updateDiaryInfo(Long id,DiaryInfo updateInfo) {
		Optional<DiaryInfo>existingDiaryInfo=diaryrepo.findById(id);
		
		if(existingDiaryInfo.isPresent()) {
			
			DiaryInfo DiaryInfotoupdate=existingDiaryInfo.get();
			
			DiaryInfotoupdate.setGrnNO(updateInfo.getGrnNO());
			DiaryInfotoupdate.setStudentName(updateInfo.getStudentName());
			DiaryInfotoupdate.setInformation(updateInfo.getInformation());
			DiaryInfotoupdate.setDate(updateInfo.getDate());
			return diaryrepo.save(DiaryInfotoupdate);
			
		}
		return null;
	}
	
	public boolean deleteDiaryInfoById(Long id) {
		Optional<DiaryInfo> existingDiaryInfo = diaryrepo.findById(id);

		if (existingDiaryInfo.isPresent()) {
			diaryrepo.deleteById(id);
			return true;
		}

		return false;
	}
}
