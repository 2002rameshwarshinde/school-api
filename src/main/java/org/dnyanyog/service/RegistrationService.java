package org.dnyanyog.service;

import org.dnyanyog.Dto.Request.AdmissionRequest;
import org.dnyanyog.Dto.Request.RegistrationRequest;

import org.dnyanyog.Dto.response.RegistrationResponse;
import org.dnyanyog.Repository.StudentRepository;
import org.dnyanyog.Repository.UserRepository;
import org.dnyanyog.entity.Student;
import org.dnyanyog.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

	@Autowired
	RegistrationResponse response;

	@Autowired
	User user;

	@Autowired
	AdmissionRequest admissionRequest;

	@Autowired
	Student stud;

	@Autowired
	UserRepository userRepo;

	@Autowired
	StudentRepository studentRepo;

	public ResponseEntity<RegistrationResponse> saveData(RegistrationRequest request) {

		response = new RegistrationResponse();

		user = new User();

		user.setMobileNo(request.getMobileNo());
		user.setGrnNo(request.getGrnNo());
		user.setPassword(request.getPassword());
		user.setRole(request.getRole());

		user = userRepo.save(user);
		response.setStatus("Success");
		response.setMassage("SingnUp SucccessFull");

		return ResponseEntity.status(HttpStatus.CREATED).body(response);
	}
}
