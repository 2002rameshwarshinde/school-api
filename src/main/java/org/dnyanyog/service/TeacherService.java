package org.dnyanyog.service;

import org.dnyanyog.Dto.Request.TeacherRequest;
import org.dnyanyog.Dto.response.TeacherData;
import org.dnyanyog.Dto.response.TeacherResponse;
import org.dnyanyog.Repository.TeacherRepository;
import org.dnyanyog.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class TeacherService {

	@Autowired
	TeacherRepository repository;
	
	@Autowired 
	Teacher teacher;
	
	@Autowired
	TeacherResponse teacherresponse;
	
	@Autowired
	TeacherData teacherdata;
	
	public ResponseEntity<TeacherResponse>addTeacher(TeacherRequest request){
		
		teacherresponse=new TeacherResponse();
		teacherresponse.setData(new TeacherData());
		
		teacher=new Teacher();
		
		teacher.setName(request.getName());
		teacher.setMobileNo(request.getMobileNo());
		teacher.setAddress(request.getAddress());
		teacher.setQualification(request.getQualification());
		teacher.setSubject(request.getSubject());
		teacher.setGender(request.getGender());
		teacher.setEmail(request.getEmail());
		
		teacher=repository.save(teacher);
		
		teacherresponse.setStatus("Success");
		teacherresponse.setMessage("Teacher Added SuccessFully");
		teacherresponse.getData().setId(teacher.getId());
		teacherresponse.getData().setName(teacher.getName());
		teacherresponse.getData().setMobileNo(teacher.getMobileNo());
		teacherresponse.getData().setAddress(teacher.getAddress());
		teacherresponse.getData().setQualification(teacher.getQualification());
		teacherresponse.getData().setSubject(teacher.getSubject());
		teacherresponse.getData().setGender(teacher.getGender());
		teacherresponse.getData().setEmail(teacher.getEmail());
		
		return ResponseEntity.status(HttpStatus.CREATED).body(teacherresponse);
		
	}
}
