package org.dnyanyog.controller;

import java.util.List;

import org.dnyanyog.Dto.Request.TimetableRequest;
import org.dnyanyog.Dto.response.TimetableResponse;
import org.dnyanyog.Repository.TimetableRepository;
import org.dnyanyog.entity.Timetable;
import org.dnyanyog.service.Timetableservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RestController;

@RestController

public class TimetableController {

	@Autowired
     Timetableservice timetableService;

    @Autowired
    TimetableRepository repository;

    @GetMapping
    public List<Timetable> getAllTimetableEntries() {
        return timetableService.getAllTimetableEntries();
    }
    
    

    @GetMapping(path="Timetable/api/{className}")
    public List<Timetable> getTimetableEntriesByClass(@PathVariable String className) {
        return timetableService.getTimetableEntriesByClass(className);
    }
    
    
    

    @PostMapping(path="Timetable/api/add")
    public ResponseEntity<TimetableResponse> createTimetable(@RequestBody TimetableRequest request) {
     
        return timetableService.createTimetable(request);
    }
    
    
    
    @DeleteMapping(path="/Timetable/Api/delete/{className}")
    public ResponseEntity<String> deleteTimetableByClassName(@PathVariable String className) {
        try {
           
            boolean deleted = timetableService.deleteTimetableByClassName(className);

            if (deleted) {
                return ResponseEntity.ok("Timetable entry with className " + className + " deleted successfully.");
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Timetable entry with className " + className + " not found.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occurred while deleting timetable entry.");
        }
    }
    
    
    
    
    
    @PutMapping("/Timetable/Api/update/{id}")
    public ResponseEntity<Timetable> updateTimetable(@PathVariable Long id, @RequestBody Timetable updatedTimetable) {
        Timetable updatedEntity = timetableService.updateTimetable(id, updatedTimetable);
        if (updatedEntity != null) {
            return new ResponseEntity<>(updatedEntity, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}

