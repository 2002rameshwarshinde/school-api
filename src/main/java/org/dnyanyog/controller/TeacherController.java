package org.dnyanyog.controller;

import org.dnyanyog.Dto.Request.TeacherRequest;
import org.dnyanyog.Dto.response.TeacherResponse;
import org.dnyanyog.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeacherController {

	
	@Autowired
	TeacherService teacherservice;
	
	@PostMapping(path="/Teacher/api/add")
	public ResponseEntity<TeacherResponse>addTeacher(@RequestBody TeacherRequest request){
		
		return teacherservice.addTeacher(request);
	}
}
