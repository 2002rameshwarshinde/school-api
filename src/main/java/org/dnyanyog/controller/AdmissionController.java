package org.dnyanyog.controller;

import org.dnyanyog.Dto.Request.AdmissionRequest;
import org.dnyanyog.Dto.response.AdmissionResponse;
import org.dnyanyog.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdmissionController {

	@Autowired
	StudentService service;

	@PostMapping(path = "admission/api/v1/add")
	public ResponseEntity<AdmissionResponse> addStudent(@RequestBody AdmissionRequest request) {
		return service.SaveData(request);
	}
}