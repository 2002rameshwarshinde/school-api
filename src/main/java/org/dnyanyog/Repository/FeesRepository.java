package org.dnyanyog.Repository;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.FeesInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface FeesRepository extends JpaRepository<FeesInfo, Long> {

    List<FeesInfo> findByGrnNo(Long grnNo);

    List<FeesInfo> findAllByStudentName(String studentName);

    List<FeesInfo> findAllByAppliedClass(String appliedClass);
    
  //  List<FeesInfo> findById(long id);
    
    boolean existsByGrnNo(Long grnNo);

    void deleteByGrnNo(Long grnNo);
    
    Optional<FeesInfo>findById(Long id);
}

	
	

 