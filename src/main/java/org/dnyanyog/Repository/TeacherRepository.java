 package org.dnyanyog.Repository;

import java.util.List;
import java.util.Optional;

import org.dnyanyog.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepository extends JpaRepository<Teacher,Long> {

	Optional<Teacher> findById(Long id);

}
